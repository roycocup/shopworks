<?php

namespace Tests\Feature;

use App\Rota;
use App\Shift;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RotasTest extends TestCase
{

    public function test_can_get_shifts_for_rota()
    {
        $rota = factory(Rota::class)->create();
        $shifts = factory(Shift::class, 2)->make([
            'rota_id' => $rota->id
        ]);

        $rota->shifts()->saveMany($shifts);


        $this->assertCount(2, $rota->shifts()->getResults());
    }

}
