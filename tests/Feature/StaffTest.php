<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Shop;
use App\Shift;
use App\Rota;
use App\Staff;
use Carbon\Carbon;

class StaffTest extends TestCase
{
    public function test_staff_is_assigned_to_a_shop()
    {
        $shop = factory(Shop::class)->create([
            'name' => 'funhouse',
        ]);
        $staff = factory(Staff::class)->make();
        $shop->staff()->save($staff);

        $this->assertEquals('funhouse', $staff->shop->name);
    }

    public function test_staff_has_shifts()
    {
        $shop = factory(Shop::class)->create();
        $staff = factory(Staff::class)->make();
        $shop->staff()->save($staff);
        $rota  = factory(Rota::class)->make();
        $rota->shop()->save($shop);

        $shift = factory(Shift::class)->make();
        $shift->rota()->save($rota);
        $shift->staff()->save($staff);

    }

}
