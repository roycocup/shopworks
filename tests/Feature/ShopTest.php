<?php

namespace Tests\Feature;

use App\Rota;
use App\Shop;
use App\Shift;
use App\ShiftBreak;
use App\Staff;
use Carbon\CarbonInterval;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;

class ShopTest extends TestCase
{

    use WithFaker;

    public function test_can_get_worked_days_for_shop()
    {
        $shop = factory(Shop::class)->create();
        $period = CarbonInterval::create(0,0,0, rand(0,8));

        // get the working days results for this period
        $workedDays = $shop->getWorkedDays($period);

        $this->assertIsArray($workedDays);
    }

    public function test_can_set_up_shop_with_rotas()
    {
        $shop = factory(Shop::class)->create([
            'name' => 'funhouse',
        ]);
        $rota1 = factory(Rota::class)->create([
            'shop_id' => $shop,
            'week_commence_date' => Carbon::now()->subdays(2)->setTime(0, 0), 
        ]);
        $rota2 = factory(Rota::class)->create([
            'shop_id' => $shop,
            'week_commence_date' => Carbon::now()->subdays(7-2)->setTime(0, 0), 
        ]);
        $rota2 = factory(Rota::class)->create([
            'shop_id' => $shop,
            'week_commence_date' => Carbon::now()->subdays(15-2)->setTime(0, 0), 
        ]);
        
        $this->assertEquals(3, $shop->rotas()->count());
    }

    

    
}
