<?php

namespace Tests\Feature;

use App\Rota;
use App\Shift;
use App\ShiftBreak;
use App\Shop;
use App\Staff;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class SingleManningTest extends TestCase
{
    use WithFaker;

    public function test_can_get_singlemanning_minutes_for_shop_this_week()
    {
        // define the shop
        $shop = factory(Shop::class)->create();
        // define a rota
        $rota = factory(Rota::class)->make([
            'week_commence_date' => $this->faker->dateTimeBetween('-7 days', '-3 days'),
        ]);
        // associate the rotas to Shop
        $shop->rotas()->save($rota);

        // "this week" is the week that starts on $shop->week_commnece_date up to now.
        $rotaStart = Carbon::parse($shop->rotas()->first()->week_commence_date);

        // the period (this week) is the difference between now and the last time this rota started
        $period = $rotaStart->diff(Carbon::now());

        // get the working days results for this period
        $workedDays = $shop->getWorkedDays($period);
        
        // we should get something back
        $this->assertNotEmpty($workedDays);

        // assert that each one of the days has an SM object associated to it.
        $this->assertNotEmpty($workedDays['days']);

        foreach($workedDays['days'] as $workedDay){
            $this->assertNotEmpty($workedDay['shifts']);
            foreach ($workedDay['shifts'] as $shift){
                $this->assertIsInt($shift['single_manning_minutes']);
            }
        }
    }

}
