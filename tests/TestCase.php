<?php

namespace Tests;

use Carbon\CarbonPeriod;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();

        CarbonPeriod::macro('getIntervalByIndex', function ($index) {
            $this->rewind();
            for($i=0; $i <= $index; $i++){
                $this->next();
            }
            return $this->current();
        });
    }
}
