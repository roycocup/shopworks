<?php

namespace Tests\Unit;

use App\Rota;
use App\Shop;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Tests\TestCase;

class ShopUnitTest extends TestCase
{
    public function test_shop_can_get_rotas_for_period()
    {
        $shop = factory(Shop::class)->create();
        $rotas = factory(Rota::class, 10)->make([
            'shop_id' => $shop->id
        ]);
        $shop->rotas()->saveMany($rotas);

        $now = Carbon::now();
        $then = Carbon::now()->subDays(7);
        $period = CarbonPeriod::create($then, '1 days', $now);

        $rotas = $shop->getRotasForPeriod($period);

        $this->assertCount(10, $rotas);
    }
}
