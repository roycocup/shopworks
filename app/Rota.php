<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rota extends Model
{

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }

    public function getSingleManning()
    {
        $sm = new SingleManning();
        return $sm;
    }

}
