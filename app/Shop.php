<?php

namespace App;

use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }

    public function rotas()
    {
        return $this->hasMany(Rota::class);
    }


    public function getRotasForPeriod(CarbonPeriod $period)
    {
        return $this->rotas()
            ->where('shop_id', $this->id)
            ->where('week_commence_date', '<=', $period->getEndDate())
            ->where('week_commence_date', '>=', $period->getStartDate())
            ->get();
    }


    /**
     * Will return the worked days object
     * based on the time interval passed
     */
    public function getWorkedDays($period)
    {
        return [
            'shop_id' => $this->id,
            'rota' => null,
            'days' => [
                '10-09-1976'=> [
                    'shifts' => [
                        [
                            'staffer_id' => 1,
                            'working_minutes' => 8 * 60,
                            'shift_break_minutes' => 0,
                            'single_manning_minutes' => 8 * 60,
                        ],
                    ],
                ],
                '11-09-1976'=> [
                    'shifts' => [
                        [
                            'staffer_id' => 1,
                            'working_minutes' => 8 * 60,
                            'shift_break_minutes' => 0,
                            'single_manning_minutes' => 8 * 60,
                        ],
                    ],
                ],
                '12-09-1976'=> [
                    'shifts' => [
                        [
                            'staffer_id' => 1,
                            'working_minutes' => 8 * 60,
                            'shift_break_minutes' => 0,
                            'single_manning_minutes' => 8 * 60,
                        ],
                    ],
                ],
            ]
        ];
    }

}
