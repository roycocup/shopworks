<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftBreak extends Model
{
    public $timestamps = false;
}
