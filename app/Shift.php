<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    public function shiftBreaks()
    {
        return $this->hasMany(ShiftBreak::class);
    }
}
