<?php

use App\Shift;
use App\ShiftBreak;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Faker\Generator as Faker;

$factory->define(ShiftBreak::class, function (Faker $faker) {
    $shift = factory(Shift::class)->create();
    $workingHours = CarbonPeriod::create($shift->start_time, '30 minute', $shift->end_time);
    $randomBreakIndex = rand(0, $workingHours->count());

    $breakStart = $workingHours->getIntervalByIndex($randomBreakIndex);
    $breakEnd = $workingHours->getIntervalByIndex($randomBreakIndex + 1);

    return [
        'shift_id' => $shift,
        'start_time' => $breakStart,
        'end_time' => $breakEnd,
    ];
});
