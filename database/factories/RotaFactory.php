<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Rota::class, function (Faker $faker) {
    $possibleStart = $faker->dateTimeBetween('-7 days', '-1 days');
    return [
        'shop_id' => factory(App\Shop::class)->create(),
        'week_commence_date' => $possibleStart,
    ];
});
