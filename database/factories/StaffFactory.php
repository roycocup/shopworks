<?php

use Faker\Generator as Faker;

$factory->define(App\Staff::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'surname' => $faker->lastName,
        'shop_id' => factory(\App\Shop::class)->create(),
    ];
});
