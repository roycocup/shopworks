<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Shift::class, function (Faker $faker) {

    $startShift = Carbon::createFromTimeString('08:00:00')->subDays(1);
    $normalShift = Carbon::createFromTimeString('16:00:00')->subDays(1);
    $doubleShift = Carbon::createFromTimeString('20:00:00')->subDays(1);
    return [
        'rota_id' => factory(\App\Rota::class)->create(),
        'staff_id' => factory(App\Staff::class)->create(),
        'start_time' => $startShift,
        'end_time' => $normalShift,
    ];
});
