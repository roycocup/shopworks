<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shop = DB::table('shops')
            ->where('name', 'funhouse')
            ->first();
        
        $staff = [
            'Black Widow', 
            'Thor', 
            'Wolverine', 
            'Gamora'
        ];

        foreach($staff as $person){
            $explodedPerson = explode(' ', $person); // :)
            $this->save(
                $explodedPerson[0], 
                ($explodedPerson[1]) ?? '',
                $shop->id
            );
        }
    }

    private function save($fName, $surname, $shopId){
        DB::table('staff')->insert([
            'first_name' => $fName,
            'surname' => $surname,
            'shop_id' => $shopId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
