# Answers

1. How long did you spend on the coding test? What would you add to your solution if you had more time?
    * I would say about 8 hours. But its kinda hard to tell as I did it over several days and worked on multiple other things between. 
2. Why did you choose PHP as your main programming language?
    * I didn't. PHP chose me to be its minion in 2007 since I had a friend asking me to do a database crud for her. PHP was the most popular language then. If I had an option now I probably would have picked Python and/or Ruby. 
3. What is your favourite thing about Laravel? 
    * Comparing against Symfony - I'd say its by far easier to test, simpler to understand and with a larger ammount of resources. 
4. What is your least favourite?
    * I'm still learning Laravel in that I have only been able to work on top of other people's work but never for too long which never left me with a greater picture of the whole framework, so it would be very pretentious of me to say something bad that would turn out to be incorrect.
5. How would you track down a performance issue in production? Have you ever had to do this?
    * So systematic chase is the only way. If youhave a performance issue, what was it that made you think that? Was it the time it took to load a page? Is it the API that has big latency? Is the production site too slow altogether? 
    The next steps would need to include reproducible and measurable steps to be able to determine what is causing the issue. If anything, there's plenty of tools out there that can help you do that job and plugging some of them into a production website is most times a matter of common sense. 
    Oh, and logs. I can't believe people in 2019 still don't start everything by looking at logs. 